<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rol extends Model
{
    use HasFactory , SoftDeletes;

    public $guarded = [];
    public function users()
    {
        return $this->belongsToMany(User::class , 'user_roles');
    }
}
