<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function auth(Request $request)
    {
        $user = User::where('email',$request->email)->first();

        if (!$user){
            return $this->unauthorized();
        }

        if (!Hash::check($request->password,$user->password)) {
            return $this->unauthorized();
        }

        $user = Auth::loginUsingId($user->id);

        $tokenResult = $user->createToken('Personal Access Token');

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer'
        ]);

    }

    public function logout(Request $request) {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    protected function unauthorized()
    {
        return response()->json([
            'message' => 'Unauthorized'
        ], 401);
    }

}
