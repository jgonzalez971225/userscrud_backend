<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\SaveUserRequest;
use App\Http\Resources\Api\V1\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Rol;
use App\Models\UserRole;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //TODO:update to paginate
        $users = User::query()->latest()->get();
        return UserResource::collection($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveUserRequest $request)
    {
        $user = User::create($request->validated());
        $roles = Rol::whereIn('id',$request->roles)->get();

        //TODO: REFACTOR TO RELATIONSHIPS CREATOR $USER->ROLES()->SOMETHING($ROLES);
        // collect($roles)->each(function($rol) use($user) {
        //     UserRole::create([
        //         'user_id' => $user->id,
        //         'role_id' => $rol->id
        //     ]);
        // });
        // $user->fresh();
        // $user->load('roles');
        return new UserResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {
        $user = User::firstWhereUuid($user);
        return new UserResource($user);
    }

    public function profile(Request $request) {
        $user = Auth::user();
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SaveUserRequest $request, $user)
    {
        $user = User::firstWhereUuid($user);
        $user->update($request->validated());
        $user->fresh();
        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {
        $user = User::firstWhereUuid($user);
        $user->delete();
        return response()->json([
            'msg' => "user deleted"
        ],204);
    }
}
