<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Rol;
class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rol = new Rol();

        $rol->create([
            'name' => 'admin',
            'description' => 'its super root'
        ]);

        $rol->create([
            'name' => 'manager',
            'description' => ''
        ]);

        $rol->create([
            'name' => 'employee',
            'description' => ''
        ]);

        $rol->create([
            'name' => 'delivery_man',
            'description' => ''
        ]);
    }
}
