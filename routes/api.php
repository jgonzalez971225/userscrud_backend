<?php

use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\RolController;
use App\Http\Controllers\Api\V1\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'],function(){

    Route::post('auth',[AuthController::class,'auth']);
    Route::post('logout',[AuthController::class,'logout'])->middleware('auth:api');

    Route::get('user-profile',[UserController::class,'profile'])->middleware('auth:api');

    Route::apiResource('users',UserController::class)->names('api.v1.users')->middleware('auth:api');
    Route::apiResource('roles',RolController::class)->names('api.v1.roles')->middleware('auth:api');
});
